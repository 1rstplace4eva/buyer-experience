---
  title: What is digital transformation?
  description: Digital transformation is the process of creating new processes or modifying existing ones with the use of digital technologies.
  twitter_image: /nuxt-images/open-graph/gitlab-blog-cover.png
  topics_header:
    data:
      title:  What is digital transformation?
      block:
        - metadata:
            id_tag: what-is-digital-transformation
          text: |
              Digital transformation is the process of using digital technologies to create new or different business processes, products, and services. It can have a far-reaching impact and is limited only by technology and the imagination of an organization.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: Digital Transformation
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: Digital transformation explained
          href: "#digital-transformation-explained"
          data_ga_name: digital transformation explained
          data_ga_location: side-navigation
          variant: primary
        - text: Why does it matter?
          href: "#why-does-digital-transformation-matter"
          data_ga_name: why does digital transformation matter
          data_ga_location: side-navigation
        - text: What drives digital transformation?
          href: "#what-drives-digital-transformation"
          data_ga_name: what drives digital transformation
          data_ga_location: side-navigation
        - text: The benefits to business
          href: "#the-benefits-to-business"
          data_ga_name: the benefits to business
          data_ga_location: side-navigation
        - text: Challenges
          href: "#digital-transformation-challenges"
          data_ga_name: digital transformation challenges
          data_ga_location: side-navigation
        - text: Strategy
          href: "#digital-transformation-strategy"
          data_ga_name: digital transformation strategy
          data_ga_location: side-navigation
        - text: Digital transformation and security
          href: "#digital-transformation-and-security"
          data_ga_name: digital transformation and security
          data_ga_location: side-navigation
        - text: Enterprise
          href: "#enterprise-digital-transformation"
          data_ga_name: enterprise digital transformation
          data_ga_location: side-navigation
        - text: Digital transformation versus innovation
          href: "#digital-transformation-versus-innovation"
          data_ga_name: digital transformation versus innovation
          data_ga_location: side-navigation
        - text: Technologies
          href: "#digital-transformation-technologies"
          data_ga_name: digital transformation technologies
          data_ga_location: side-navigation
        - text: Measuring ROI
          href: "#how-to-measure-the-roi-of-digital-transformation"
          data_ga_name: how to measure the roi of digital transformation
          data_ga_location: side-navigation
    hyperlinks:
          text: ''
          data: []

    content:
      - name: copy
        data:
          block:
            - header: Digital transformation explained
              column_size: 10
              text: |
                Digital transformation involves reimagining how processes can be improved by applying technology, making your company more competitive.

                For example, customer service is now a digitally centered process rather than a representative-centered one. Previously, customers were routed to a customer service representative (CSR) who would find resources to help the customer — a process dependent on the CSR’s skills. An issue affecting many customers, like a new software release, could result in long queues and customer dissatisfaction.

                Today’s customer service strategies rely on digital processes. Customers may refer to Frequently Asked Questions (FAQs) on a website or begin an online chat with a CSR or an automated chatbot. Digital technologies like artificial intelligence (AI) and machine learning (ML) shorten the customer journey by directing them to paths that have helped other customers.

                If the customer does speak with a CSR, their website history can be made available so that the CSR can see what the customer has already tried. All of these digital transformation efforts can significantly improve the user experience.
              margin_top: 96
      - name: copy
        data:
          block:
            - header: Why does digital transformation matter?
              column_size: 10
              text: |
                Digital transformation is an investment aimed at increasing your company's bottom line.

                Improving purchasing, payment, or browsing processes positively impacts both your customers’ experience and your revenue. Whether you’re competing for sales or providing customer service, improved processes that benefit the customer also reward your organization.

                Something as simple as being able to store a payment method for future use creates stickiness for purchasing through your applications, providing an immediate advantage over applications and companies that don’t offer it. This may also increase customer loyalty and remove a barrier to purchase completion.
      - name: copy
        data:
          block:
            - header: What drives digital transformation?
              column_size: 10
              text: |
                The main drivers are rapidly evolving business requirements and the technologies that enable organizations to meet them. Organizations must respond to business requirements, including competitor price changes, fluctuating market conditions, or compliance issues. In all instances, technology better enables organizations to meet these new challenges.

                Take, for example, the many capabilities of smartphones. An easily deployed QR code scanned by a mobile device or smartphone provides instant access to product information. When new business requirements require changes to this information, it’s easy to change the target destination of the code without having to redeploy. This way, technology accelerates the pace of change.

                As organizations adopt and adapt new technological capabilities to the business environment, they gain a competitive advantage, forcing their competitors to follow suit. Consequently, changing business requirements are both a cause and a consequence of new technology, leading to increasingly widespread digital transformation.
      - name: copy
        data:
          block:
            - header: The benefits to business
              column_size: 10
              text: |
                Digital transformation can involve many targets, like increasing efficiency, reducing overheads, reducing toil and burden on employees, automating repetitive tasks, and enhancing observability in business processes.

                One of the keys to a successful digital transformation strategy is understanding who benefits — and how they benefit — from the process change. Knowing the benefits and costs helps to calculate return on investment (ROI). If you cannot determine who benefits, you probably shouldn’t be investing.


                Using technology to improve mundane tasks reduces the burden on people. This change not only enhances customer satisfaction but increases employee and CSR job satisfaction — a compelling factor for what is typically a high-turnover position.

                By enhancing the observability of processes, an organization can collect data to improve that process or other areas of the business.
      - name: copy
        data:
          block:
            - header: Digital transformation challenges
              column_size: 10
              text: |
                In addition to the usual barriers of making people change existing processes, there are the difficulties of adopting new and unfamiliar technologies. There’s often a steep learning curve when getting a team up to speed with unfamiliar technologies, necessitating training, hiring, or outsourcing.

                New additions to your technology stack also increase your maintenance burden. New tools may require changing your workflows or business processes to accommodate them. For digital transformation initiatives to succeed, the cost and difficulty of change must be assessed and challenges mitigated.
      - name: copy
        data:
          block:
            - header: Digital transformation strategy
              column_size: 10
              text: |
                Strategy is about knowing where you are going. While all strategies must have missions and objectives, digital transformation strategies must incorporate additional elements. These include which sections of the organization can benefit the most from digitization, whether the technology is available to support your goals, and how workflows and processes can be adapted to take advantage of technology.

                For a complete discussion of the digital transformation strategy, see GitLab’s [Digital Experience Handbook](https://about.gitlab.com/handbook/marketing/digital-experience/) or [Gartner’s](https://emtemp.gcom.cloud/ngw/globalassets/en/information-technology/documents/insights/the-gartner-it-roadmap-for-digital-buisness-transformation-excerpt.pdf) take on digital strategy.
      - name: copy
        data:
          block:
            - header: Digital transformation and security
              column_size: 10
              text: |
                With digital transformation and a more complex technology stack comes an increase in the exchange of sensitive information across systems, tools, and data sources. Users must be confident that their information is secure before using digital processes.

                Increased data movement means more connections. Since every connection can be vulnerable to bad actors, digital transformation increases security risks and the importance of security.

                To maintain users’ trust, technology providers must maintain numerous certifications. For example, GitLab maintains both ISO and SOC certifications. See GitLab’s complete list of [security certifications](https://about.gitlab.com/security/) to learn more.
      - name: copy
        data:
          block:
            - header: Enterprise digital transformation
              column_size: 10
              text: |
                While the digital transformation of a single process may sound manageable, there are usually thousands of processes in large enterprises, each interacting with many others. This creates a change in perspective, from managing a set of technology tools to delivering a set of services, and poses the problem of ensuring that the technology stack can support the organization’s business needs.

                Information Technology service management (ITSM) provides a framework of policies, processes, and procedures for managing customer- and business-oriented IT services. Unlike traditional IT management, which focuses on components like hardware, storage, and networks, ITSM focuses on how these elements work together to provide cohesive service delivery. Learn more about ITSM in this [overview](https://www.cio.com/article/230741/what-is-itsm-managing-it-to-serve-business-needs.html) provided by CIO Magazine.

                Enterprise service management (ESM) is the part of ITSM that focuses on visibility and access to enterprise services, specifically on speeding the delivery and automation of ITSM services. These services might include automating provisioning, incident response, ticketing, and service desk operations. [GitLab Service Desk](https://about.gitlab.com/blog/2017/05/09/demo-service-desk/) is an example of how ESM can be implemented for a service desk. Both ITSM and ESM provide a structure for successfully conducting and maintaining digital transformation across the enterprise.
      - name: copy
        data:
          block:
            - header: Digital transformation versus innovation
              column_size: 10
              text: |
                Digital transformation is the process of applying technology to improve a business process, whereas innovation is the end result.

                Digital transformation is about applying technical solutions. It’s the act of building the technical solution. For example, consider DevOps teams who develop and deploy a new application, and the application of network and technical architecture.

                Innovation, on the other hand, is more about the “why” than the “how.” Innovation shapes the desired outcome of the transformation.
      - name: copy
        data:
          block:
            - header: Digital transformation technologies
              column_size: 10
              text: |
                Digital transformation success depends on a variety of tools. For example, for teams to work together — especially when geographically dispersed — they require collaboration and project management tools. Collaboration tools include chat or video conferencing applications and tools to categorize communications into various groups. Project management tools keep teams on track, coordinate resources, and provide project transparency for leaders and people across various roles.

                Access to the cloud provides a run-time environment and the ability to test different software approaches without investing in licenses. Creating realistic environments efficiently, without acquisition delays, also makes digital transformation more efficient.

                Additionally, ITSM tools assist infrastructure and operations groups in supporting the production environment. These tools may monitor the health of the infrastructure and automate or improve workflows to help maintain a trouble-free environment.
      - name: copy
        data:
          block:
            - header: How to measure the ROI of digital transformation
              column_size: 10
              text: |
                Digital transformation is an investment — with all associated costs and benefits — in your organization’s future. Determining the overall cost of digital transformation means identifying and estimating all the costs associated with the project. These likely include the usual list of project costs consisting of personnel, training, and infrastructure requirements.

                Determining the returns and benefits is trickier. To determine the benefits, return to the digital transformation project’s goals. Identifying all the areas that will benefit can be difficult, and it’s even harder to anticipate how great the benefit will be and how to monetize or measure it.

                Furthermore, ROI may change over the lifetime of the transformation, so it’s necessary to establish ROI measurement as a process rather than a single event. Sometimes you need to establish a range of possible outcomes and the probabilities of each to create an analysis of the ROI.
