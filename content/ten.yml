title: GitLab turns 10!
description: We're celebrating ten years since the first commit to the GitLab open source project.
image_title: /nuxt-images/ten/gitlab-10-year-og-image.png
navigation_block:
  brand_link: '/ten/'
  cta_link: '/free-trial/'
  cta_text: 'Get Free Trial'
  location: 'October 8, 2021'
  date:
  links:
    - title: Thank you!
      href: "#thank-you"
    - title: Contribute
      href: '#contribute'
    - title: Let's Party
      href: '#lets-party'
    - title: Timeline
      href: '#timeline'
    - title: Fun facts
      href: '#fun-facts'
    - title: Join in
      href: '#join-in'
    - title: Stay in touch
      href: '#stay-in-touch'
hero_image_block:
  overline_text:
  big_text: GitLab turns 10!
  subcopy: We're celebrating ten years since the first commit to the GitLab open source project.
  primary_cta_text: Get Free Trial
  primary_cta_link: /free-trial/
  image: /nuxt-images/ten/hero.svg
  image_alt_text: "The number 10 surrounded by confetti"
contribute_block:
  video:
    displace_left: true
    size: md
    video_id: -JUtVOAaQcE
    video_url: https://player.vimeo.com/video/625794403?h=7d22778cf4
    text: Our 10 year milestone would not be possible without your contributions. Thank you for being part of our journey!
  image_and_content:
    header: Contribute
    subtitle: Co-create GitLab with us.
    text: |
      **GitLab is committed to the [stewardship](/company/stewardship/){data-ga-name="stewardship" data-ga-location="body"} of the open source project. Help us build the future iteration to enable your own projects to thrive!**

      We value all types of contributions. Whether you're a project manager, writer, designer, developer, translator, or excited human being &mdash; we'd love to create GitLab alongside you!
    link_text: Learn more
    link_href: /community/contribute/
    image: /nuxt-images/ten/contribute.svg
    image_alt_text: "Grapich of photo album"
  quotes:
    - image: /nuxt-images/ten/gitlab-10-year-quote-marvin.png
      image_alt_text: Headshot of Marvin Karegyeya
      quote: I've been able to advance my skills with the contributions I've been making to the GitLab code base. I've grown professionally and of course I've also grown in terms of having better relationships."
      name: Marvin Karegyeya
      title: GitLab Hero
      link: /community/heroes/members/
    - image: /nuxt-images/ten/gitlab-10-year-quote-akanksha.png
      image_alt_text: Headshot of Akanksha Bhasin
      quote: The best part of the GitLab community is the support and the network."
      name: Akanksha Bhasin
      title: GitLab Hero
      link: /community/heroes/members/
    - image: /nuxt-images/ten/gitlab-10-year-quote-lee.png
      image_alt_text: Headshot of Lee Tickett
      quote: I value the relationships I’ve built with GitLab team members, many even feel like mentors. They've taught me so much, and I appreciate the time they take out of their day to chat with me, whether it be work related or otherwise. I love the approach and the mentality."
      name: Lee Tickett
      title: Core Team
      link: /community/core-team/
lets_party_block:
  header: Let's Party
  subtitle: Join an existing virtual event or meetup.
  description_text: Don't see a virtual meetup in your area? Help us host one! [Learn more](/community/ten-year/){data-ga-name="learn more" data-ga-location="body"}
  events:
    - heading: Norrokoping Software Development Meetup
      icon: calendar
      date: October 7, 2021
      event_type: Meetup
      cta: This meetup will feature discussions about best practices, architecture exploration of the tool and celebrate the 10 year milestone of GitLab's first commit. It will be a great setting to chat and learn from each other while having some fun with code.
      destination_url: 'https://www.meetup.com/Norrkoping-Software-Development/events/280619464/'
    - heading: Taipei Meetup
      icon: calendar
      date: October 8, 2021
      event_type: Meetup
      cta:
      destination_url: https://www.meetup.com/GitLab-Meetup-Taipei/events/281063073/
    - heading: NorthCyberSec + eHealth Africa + GitLab Meetup
      icon: calendar
      date: October 23, 2021
      event_type: Meetup
      cta: The NorthCyberSec Is a meetup happening every month in Kano, Nigeria that brings technical and non-technical cybersecurity hobbyists together. This event will be unique as they have planned some special talks to talk DevOpsSec and celebrating GitLab's 10th anniversary since the first commit.
      destination_url: 'https://www.eventbrite.com/e/northcybersec-privacy-is-not-an-option-tickets-181380232157'
    - heading: France GitLab Meetup
      icon: calendar
      date: October 11, 2021
      event_type: Virtual event
      cta:
      destination_url: 'https://www.meetup.com/GitLab-Meetup-France/events/281270178/'
    - heading: London GitLab Meetup Group
      icon: calendar
      date: October 13, 2021
      event_type: Virtual event
      cta: In celebration of GitLab's 10th Anniversary since the first commit we are joining together to talk a little about how GitLab has changed/evolved and what that has meant for you and your teams. The host is asking if attendees could come with an idea of a 60-seconds chat of what GitLab has done for you.
      destination_url: 'https://www.meetup.com/London-Gitlab-Meetup-Group/events/281042106/'
    - heading: Tokyo GitLab Meetup
      icon: calendar
      date: October 15, 2021
      event_type: Virtual event
      cta: The Tokyo meetup group is hosting a meetup to celebrate GitLab's 10th anniversary since the first commit. There be a few lightning talks including topics DevOps. It is ideal for those who want to start using Git, introduce GitLab, and want to make more use of GitLab.
      destination_url: https://gitlab-jp.connpass.com/event/224530/
    - heading: 10 Year Anniversary  + COPS IIT BHU (India)
      icon: calendar
      date: October 13, 2021
      event_type: Virtual event
      cta: A speaker session organized by COPS IIT BHU in collaboration with GitLab to provide you with deeper insights into DevOps.
      destination_url: 'https://gdsc.community.dev/events/details/developer-student-clubs-indian-institute-of-technology-varanasi-presents-devops-with-gitlab/'
    - heading: Celebrating 10 Years of GitLab in Istanbul
      icon: calendar
      date: October 15, 2021
      event_type: Virtual event
      cta:
      destination_url: https://www.meetup.com/ui-ux-istanbul-gitlab/events/281361679/
    - heading: Celebrating 10 Years of GitLab with XStream Data (Italy)
      icon: calendar
      date: October 26, 2021
      event_type: Virtual event
      cta: Xsteam will dedicating the next meetup to the integrated DevOps platform, based on Git, which facilitates collaborative software development.
      destination_url: 'https://www.meetup.com/it-IT/Xstream-Data/events/281297158/?isFirstPublish=true'
    - heading: "Let's Party: GitLab turns 10! hosted by GitLab"
      icon: calendar
      date: October 27, 2021
      event_type: Virtual event
      cta: Join in the fun with GitLab team members as we celebrate the 10 years since the first commit. Grab your favorite snack or caffeinated beverage and let's toast to continued success. Listen to some music and chat with GitLab team members and members of the wider community.
      destination_url: https://www.meetup.com/gitlab-virtual-meetups/events/281381663/?isFirstPublish=true
timeline_block:
  header: Timeline
  subtitle: It's been 10 years since the first commit to the GitLab open source project.
  milestones:
    - icon: start
      title: October 8, 2011
      text: Dmitriy Zaporozhets starts GitLab as an open source project with [this commit](https://gitlab.com/gitlab-org/gitlab-foss/-/commit/9ba1224867665844b117fa037e1465bb706b3685){data-ga-name="this commit" data-ga-location="body"}.
    - icon: node
      title: '2012'
      text: Sid Sijbrandij discovers GitLab and decides to bootstrap a company around it using money he earned from crypto investments. He hires Marin Jankovski [as the first GitLab team member](/blog/2012/09/04/welcome-marin/){data-ga-name="first gitlab team member" data-ga-location="body"}, and later invites Dmitriy to join as a co-founder. Hundreds of people sign up for GitLab beta after Sid [posts in the Hacker News community](https://news.ycombinator.com/item?id=4428278).
    - icon: node
      title: 2013
      text: The first GitLab Summit is held in Novi Sad, Serbia. A highlight is [lunch at Marin’s mom's home](/nuxt-images/ten/GitLab_Summit_Dinner.png). The GitLab Summit soon evolves into GitLab's annual team conference, and is called "Contribute" in future iterations.
    - icon: node
      title: 2014
      text: '[GitLab Cloud becomes GitLab.com](/blog/2014/04/18/gitlab-cloud-becomes-gitlab-com/){data-ga-name="git lab cloud becomes gitlab.com" data-ga-location="body"}. Soon after, GitLab is incorporated as a limited company on September 10, 2014.'
    - icon: node
      title: 2015
      text: |
        Kamil Trzciński’s [unofficial runner becomes official](/blog/2015/05/03/unofficial-runner-becomes-official/){data-ga-name="unnofficial runner" data-ga-location="body"}. He began developing it as a way to learn Go and contributed it as an unofficial GitLab project. Sid and Dmitriy love it and offer him a job.
    - icon: node
      title: '2016'
      text: 'GitLab Source Code Management (SCM) and Continuous Integration (CI) are combined into a [single application](/blog/2016/08/05/continuous-integration-delivery-and-deployment-with-gitlab/){data-ga-name="single application" data-ga-location="body"}.'
    - icon: node
      title: 2017
      text: |
        GitLab switches to a Developer Certificate of Origin (DCO) for Community Edition contributions. This helps reinforce GitLab's commitment to its [stewardship of the open source project](/company/stewardship/){data-ga-name="stewardship of the open source project" data-ga-location="body"} by giving developers greater flexibility and portability for their contributions.
    - icon: node
      title: '2018'
      text: |
        [GNOME moves to GitLab](/blog/2018/05/31/welcome-gnome-to-gitlab/) and sets a precedent for large open source organizations migrating. [The Drupal Association](/blog/2018/08/16/drupal-moves-to-gitlab/){data-ga-name="drupal" data-ga-location="body"}, and others, soon follow. The [GitLab for Open Source](/solutions/open-source/){data-ga-name="open source" data-ga-location="body"}, [GitLab for Education](/solutions/education/){data-ga-name="education" data-ga-location="body"}, and [GitLab for Startups](/solutions/startups/){data-ga-name="startups" data-ga-location="body"} programs are created to offer GitLab's top tier and 50,000 CI minutes for free to qualifying organizations and projects.
    - icon: node
      title: '2019'
      text: 'GitLab holds its first user conference, [GitLab Commit, in Brooklyn, New York](/events/commit/){data-ga-name="brooklyn" data-ga-location="body"}. Another GitLab Commit is [held in London](/events/commit/){data-ga-name="london" data-ga-location="body"} later in the year.'
    - icon: node
      title: '2020'
      text: 'The [GitLab Open Source Partners program](/solutions/open-source/partners/){data-ga-name="open source partners" data-ga-location="body"} is launched to collaborate on making GitLab better for all open source communities.'
    - icon: node
      title: 2021
      text: 'The GitLab community grows to over 2,600 contributors as of July 31, 2021. [GitLab participates in Google Summer of Code](/blog/2021/09/01/gsoc-at-gitlab/){data-ga-name="google summer of code" data-ga-location="body"} and [Hacktoberfest](/blog/2021/10/01/join-us-for-hacktoberfest-2021/){data-ga-name="hacktoberfest" data-ga-location="body"} for the first time. The GitLab for Education Program reaches one million users.'
    - icon: node
      title: October 8, 2021
      text: 'GitLab celebrates its 10 year anniversary. [Share your favorite memories](/community/ten-year/#show--tell--share-your-story){data-ga-name="favorite memories" data-ga-location="body"} from "10 years of GitLab".'
ten_fun_facts_block:
  header: 10 fun facts about GitLab.
  image: /nuxt-images/ten/gitlab-10-year-trivia-cards.svg
  image_alt_text: SVG showing trivia cards
  facts:
    - 'Our logo depicts a [Tanuki](https://en.wikipedia.org/wiki/Japanese_raccoon_dog){data-ga-name="tanuki" data-ga-location="body"}, a Japanese racoon dog. On social media, we use a fox emoji, and we also have a Font Awesome icon that we often use: [fa-gitlab](https://fontawesome.com/v4.7/icon/gitlab).'
    - 'Over 10 people who earned the GitLab [MVP award](/community/mvp/){data-ga-name="mvp award" data-ga-location="body"} as contributors have ended up working at GitLab Inc.'
    - 'The co-founders, Sid Sijbrandij and Dmitriy Zaporozhets, and GitLab’s first employee,  Marin Jankovski, met in real life for the first time at [Railsberry, Krakow 2013](/nuxt-images/ten/railsberry.jpg).'
    - 'GitLab’s company robot, named Beamy, lived in our San Francisco boardroom (which is in Sid’s home in the city).'
    - 'As of July 31, 2021, over ninety people in the wider GitLab community earned the [GitLab Hero](/community/heroes/members/){data-ga-name="gitlab hero" data-ga-location="body"} title for their important contributions.'
    - 'We have over 2,000 web pages in the [GitLab handbook](/handbook/){data-ga-name="handbook" data-ga-location="body"} to enable our fully remote team to work transparently and collaborate more efficiently across the world.'
    - 'Our tanuki logo has evolved over the years from something that people said looked a little scary, [into the lovable icon we have today](/blog/2015/05/18/a-new-gitlab-logo/){data-ga-name="gitlab logo" data-ga-location="body"}.'
    - 'As of July 31, 2021, GitLab has over 1,350 [team members](/company/team/){data-ga-name="team members" data-ga-location="body"} in over 65 countries with zero company-owned offices.'
    - |
      GitLab's [Team Member Resource Groups (TMRGs)](/company/culture/inclusion/erg-guide/){data-ga-name="TMRGs" data-ga-location="body"} help to foster diversity, inclusion and belonging within GitLab.
    - 'The [core team](/community/core-team/){data-ga-name="core team" data-ga-location="body"}, a group of people who represent the wider GitLab community and help GitLab live up to its [mission](/company/mission/){data-ga-name="mission" data-ga-location="body"} and [values](/handbook/values/){data-ga-name="values" data-ga-location="body"}, hosted the first ["Community Weekend" Hackathon](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/uploads/4052f158afc977415fa7d9e761c9f54f/GITLAB__1_.pdf){data-ga-name="hackathon" data-ga-location="body"} in 2015 where they met virtually to fix bugs and triage issues. Since that first event, GitLab has continued to host [quarterly virtual hackathons](/community/hackathon/){data-ga-name="quarterly virtual hackathons" data-ga-location="body"}.'
join_us_block:
  copy_image_block_1:
    header: Join In
    subtitle: Help us celebrate
    text: |
      We'll be celebrating GitLab's 10 year anniversary throughout the year and into 2022 &mdash; and there are many ways to join the fun!

      We encourage you to join a virtual meetup, write a blog post, share your story with us, or just help us amplify the ten year celebrations! #10YearsOfGitLab
    link_text: Learn more
    link_href: /community/ten-year/
    image: /nuxt-images/ten/gitlab-10-year-cake.svg
    image_alt_text: Placeholder image for contribute content
  copy_image_block_2:
    header:
    subtitle: Free Community Programs
    text: We offer our top tier and 50,000 CI minutes for free to qualifying open source projects, educational institutions, and startups.
    link_text: Learn more
    link_href: /community/
    image: /nuxt-images/ten/gitlab-10-year-open-sign.svg
    image_alt_text: Placeholder image for contribute content
get_in_touch_email_acquisition_block:
  header: Stay in touch
  subtitle: Keep up to date with what’s happening at GitLab.
  content: Join our mailing list to learn about upcoming events, product updates, and community news. You can unsubscribe at any time.
  disclaimer_text: I agree that GitLab may contact me via email about its product, services, and events.
