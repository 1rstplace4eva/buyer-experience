## 1.0.0 UX Changelog Naming Convention 
**Feb 13, 2023 - Feb 26, 2023**

### ♻️ added

* Create a [design changelog](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/merge_requests/1709/)

