{
  "name": "gitlab-core-marketing-site",
  "version": "1.6.0",
  "private": true,
  "scripts": {
    "dev": "nuxt",
    "build": "nuxt build",
    "start": "nuxt start",
    "generate": "nuxt generate --fail-on-error",
    "lint:js": "eslint --ext \".js,.vue\" --ignore-path .gitignore .",
    "lint:prettier": "prettier --check .",
    "lint": "yarn lint:js && yarn lint:prettier",
    "lintfix": "prettier --write --list-different . && yarn lint:js --fix",
    "postinstall": "husky install && ./scripts/sync-data.sh",
    "test": "jest",
    "sync-data": "./scripts/sync-data.sh",
    "find-broken-links": "node ./scripts/find-broken-links.mjs",
    "get-week": "node ./scripts/get-week.mjs"
  },
  "dependencies": {
    "@braid/vue-formulate": "^2.5.2",
    "@nuxt/content": "^1.14.0",
    "@nuxtjs/component-cache": "^1.1.6",
    "@nuxtjs/gtm": "^2.4.0",
    "@nuxtjs/markdownit": "^2.0.0",
    "@nuxtjs/sitemap": "^2.4.0",
    "@semantic-release/changelog": "^6.0.1",
    "@semantic-release/commit-analyzer": "^9.0.2",
    "@semantic-release/git": "^10.0.1",
    "@semantic-release/gitlab": "^9.5.0",
    "@semantic-release/npm": "^9.0.1",
    "@semantic-release/release-notes-generator": "^10.0.3",
    "aos": "^2.3.4",
    "be-navigation": "3.2.0",
    "conventional-changelog-conventionalcommits": "^5.0.0",
    "core-js": "^3.15.1",
    "floating-vue": "^1.0.0-beta.14",
    "ipx": "^0.9.10",
    "launchdarkly-js-client-sdk": "2.22.1",
    "lodash": "^4.17.21",
    "markdown-it-anchor": "5.0.1",
    "markdown-it-attrs": "^2.3.2",
    "markdown-it-multimd-table": "^4.1.3",
    "nuxt": "^2.15.7",
    "semantic-release": "^19.0.5",
    "slippers-ui": "^2.1.31",
    "vue-autosuggest": "^2.2.0",
    "vue-mermaid-string": "^2.2.5",
    "vue-pluralize": "^0.0.2",
    "vue-slick-carousel": "^1.0.6"
  },
  "devDependencies": {
    "@babel/eslint-parser": "^7.14.7",
    "@gitlab/eslint-plugin": "^11.0.0",
    "@nuxt/image": "^0.7.1",
    "@nuxt/types": "^2.15.7",
    "@nuxt/typescript-build": "^2.1.0",
    "@nuxtjs/eslint-config-typescript": "^6.0.1",
    "@nuxtjs/eslint-module": "^3.0.2",
    "@nuxtjs/google-fonts": "^3.0.0-0",
    "@nuxtjs/style-resources": "^1.2.1",
    "@types/aos": "^3.0.4",
    "@types/lodash": "^4.14.180",
    "@types/webpack-env": "^1.16.3",
    "axios": "^1.1.3",
    "dotenv": "^16.0.3",
    "eslint": "^7.29.0",
    "eslint-config-prettier": "^8.3.0",
    "eslint-plugin-nuxt": "^2.0.0",
    "eslint-plugin-vue": "^7.12.1",
    "eslint-plugin-custom-rules": "file:eslint-custom-rules",
    "husky": "^7.0.4",
    "jest": "^28.1.1",
    "linkinator": "^4.0.3",
    "prettier": "^2.3.2",
    "sass": "^1.38.1",
    "sass-loader": "10"
  },
  "release": {
    "branches": [
      "main"
    ],
    "plugins": [
      [
        "@semantic-release/commit-analyzer",
        {
          "releaseRules": [
            {
              "breaking": true,
              "release": "major"
            },
            {
              "type": "feat",
              "release": "minor"
            },
            {
              "type": "refactor",
              "release": "patch"
            },
            {
              "type": "docs",
              "release": "patch"
            },
            {
              "type": "test",
              "release": "patch"
            },
            {
              "type": "style",
              "release": "patch"
            },
            {
              "type": "perf",
              "release": "patch"
            },
            {
              "type": "build",
              "release": "patch"
            },
            {
              "type": "content",
              "release": "patch"
            },
            {
              "type": "chore",
              "release": false
            }
          ]
        }
      ],
      [
        "@semantic-release/release-notes-generator",
        {
          "preset": "conventionalcommits",
          "presetConfig": {
            "types": [
              {
                "type": "feat",
                "section": "🚀 Features",
                "hidden": false
              },
              {
                "type": "fix",
                "section": "🐜 Bug Fixes",
                "hidden": false
              },
              {
                "type": "docs",
                "section": "📝 Documentation",
                "hidden": false
              },
              {
                "type": "style",
                "section": "🎨 Styling",
                "hidden": false
              },
              {
                "type": "refactor",
                "section": "♻️ Refactors",
                "hidden": false
              },
              {
                "type": "perf",
                "section": "⚡ Performance Improvements",
                "hidden": false
              },
              {
                "type": "test",
                "section": "🧪 Tests",
                "hidden": false
              },
              {
                "type": "content",
                "section": "🗂️ Content change",
                "hidden": false
              },
              {
                "type": "chore",
                "hidden": true
              }
            ]
          }
        }
      ],
      "@semantic-release/npm",
      [
        "@semantic-release/changelog",
        {
          "changelogFile": "CHANGELOG.md"
        }
      ],
      [
        "@semantic-release/git",
        {
          "assets": [
            "CHANGELOG.md",
            "package.json"
          ],
          "message": "Release: ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}"
        }
      ]
    ],
    "preset": "conventionalcommits"
  }
}
